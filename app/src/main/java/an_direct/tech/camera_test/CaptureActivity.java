package an_direct.tech.camera_test;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.File;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ruslan on 18.11.14.
 */
public class CaptureActivity extends Activity {

    CameraPreview preview;
    Camera mCamera;
    FrameLayout mFrame;
    Context mContext;
    Timer mTimer;
    MyTimerTask send;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mTimer = new Timer();
        send = new MyTimerTask();
        mTimer.schedule(send, 1000, 3000);
        mContext = this;
    }

    private Camera openCamera() {
       Camera cam = null;
        if (Camera.getNumberOfCameras() > 0) {
            try {
                cam = Camera.open(1);
            }
            catch (Exception exc) {
                //
            }
        }
        return cam;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera != null){
            mCamera.stopPreview();
            mCamera.release();
            mFrame.removeView(preview);
            mCamera = null;
            preview = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera = openCamera();
        Toast.makeText(mContext, "Nice.", Toast.LENGTH_LONG).show();
        if (mCamera == null) {
            Toast.makeText(this, "Open camera failed", Toast.LENGTH_LONG).show();
            return;
        }

        preview = new CameraPreview (this, mCamera);
        mFrame = (FrameLayout) findViewById(R.id.layout);
        mFrame.addView(preview, 0);
    }

    private PictureCallback mPictureCallback = new PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

                Uri pictureFile = generateFile();
                try {
                    savePhotoInFile (data, pictureFile);
                } catch (Exception e){
                    Toast.makeText(mContext, "Error: cannot save picture", Toast.LENGTH_LONG).show();
                }
                mCamera.startPreview();
            }
    };

    private Uri generateFile () {
        File path = new File(Environment.getExternalStorageDirectory(), "AN-Security");
        if (!path.exists()){
            if (!path.mkdirs()){
                return null;
            }
        }

        String timeStamp = String.valueOf(System.currentTimeMillis());
        File newFile = new File(path.getPath() + File.separator + timeStamp + ".jpg");
        return Uri.fromFile(newFile);
    }

    private void savePhotoInFile(byte[] data, Uri pictureFile) throws Exception {
        if (pictureFile == null) throw new Exception();
        OutputStream os = getContentResolver().openOutputStream(pictureFile);
        os.write(data);
        os.close();
    }

    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCamera.takePicture(null, null, null, mPictureCallback);
                }
            });
        }
    }
}

